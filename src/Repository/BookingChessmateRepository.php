<?php

namespace App\Repository;


use App\Entity\BookingChessmate;
use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method BookingChessmate|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingChessmate|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingChessmate[]    findAll()
 * @method BookingChessmate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingChessmateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingChessmate::class);
    }

    public function findLastBooking($booking_object, $limit)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.booking_object = :booking_object')
                ->setParameter('booking_object', $booking_object)
                ->setMaxResults($limit)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findActualBookingByClient($tenant)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.tenant = :tenant')
                ->setParameter('tenant', $tenant)
                ->andWhere('a.checkOut > CURRENT_DATE()')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}
