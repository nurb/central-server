<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findOneByPassportAndEmail(string $passport, string $email): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.passport = :passport')
                ->orWhere('a.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getOneByEmailAndPassword(string $email, string $password): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->andWhere('a.password = :password')
                ->setParameter('email', $email)
                ->setParameter('password', $password)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByEmail(string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByVk($vk): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.vkId = :vkId')
                ->setParameter('vkId', $vk)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByGoogle($google): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->Where('a.googleId= :gooleId')
                ->setParameter('gooleId', $google)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByFacebook($facebook): ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->Where('a.faceBookId= :facebookId')
                ->setParameter('facebookId', $facebook)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
