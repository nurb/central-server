<?php

namespace App\Repository;

use App\Entity\BookingObject;
use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function findOneByName(string $name): ?BookingObject
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.name = :name')
                ->setParameter('name', $name)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneById(string $id): ?array
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.id = :id')
                ->setParameter('id', $id)
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function getAllObjectByCriteria(string $name, string $type, int $min_rate, int $max_rate): ?array
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.name like :name')
                ->setParameter('name', '%' . $name . '%')
                ->andWhere('a.type like :type')
                ->setParameter('type', '%' . $type . '%')
                ->andWhere('a.rates BETWEEN :min_rate AND :max_rate')
                ->setParameter('min_rate', $min_rate)
                ->setParameter('max_rate', $max_rate)
                ->setMaxResults(100)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}
