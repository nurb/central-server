<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Cottage extends BookingObject
{

    /**
     * @var string
     * @ORM\Column(type="boolean", length=128)
     */
    protected $kitchen;

    /**
     * @var string
     * @ORM\Column(type="boolean", length=128)
     */
    protected $terrace;

    /**
     * @param bool $terrace
     * @return Cottage
     */
    public function setTerrace(bool $terrace): Cottage
    {
        $this->terrace = $terrace;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTerrace(): ?bool
    {
        return $this->terrace;
    }

    /**
     * @param bool $kitchen
     * @return Cottage
     */
    public function setKitchen(bool $kitchen): Cottage
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    /**
     * @return bool
     */
    public function isKitchen(): ?bool
    {
        return $this->kitchen;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'contact' => $this->getContact(),
            'address' => $this->getAddress(),
            'rates' => $this->getRates(),
            'number_of_rooms' => $this->getNumberOfRooms(),
            'type' => $this->getType(),
            'kitchen' => $this->isKitchen(),
            'terrace' => $this->isTerrace(),
            'bookings' => $this->getBookings()
        ];
    }

}