<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingChessmateRepository")
 */
class BookingChessmate
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="check_in", type="datetime")
     */
    private $checkIn;

    /**
     *
     * @ORM\Column(name="check_out", type="datetime")
     */
    private $checkOut;

    /**
     * @var BookingObject
     * @ORM\ManyToOne(targetEntity="App\Entity\BookingObject", inversedBy="id")
     */
    private $booking_object;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="id")
     */
    private $tenant;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param $checkIn
     * @return BookingChessmate
     */
    public function setCheckIn($checkIn): BookingChessmate
    {
        $this->checkIn = $checkIn;
        return $this;
    }

    /**
     * @param $checkOut
     * @return BookingChessmate
     */
    public function setCheckOut ($checkOut): BookingChessmate
    {
        $this->checkOut = $checkOut;
        return $this;
    }

    /**
     * @param BookingObject $booking_object
     * @return BookingChessmate
     */
    public function setBookingObject(BookingObject $booking_object): BookingChessmate
    {
        $this->booking_object = $booking_object;
        return $this;
    }

    /**
     * @param Client $tenant
     * @return BookingChessmate
     */
    public function setTenant(Client $tenant): BookingChessmate
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCheckIn(): DateTime
    {
        return $this->checkIn;
    }

    /**
     * @return DateTime
     */
    public function getCheckOut(): DateTime
    {
        return $this->checkOut;
    }

    /**
     * @return BookingObject
     */
    public function getBookingObject(): BookingObject
    {
        return $this->booking_object;
    }

    /**
     * @return Client
     */
    public function getTenant(): Client
    {
        return $this->tenant;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'check-in' => $this->getCheckIn()->getTimestamp(),
            'check-out' => $this->getCheckOut()->getTimestamp(),
            'tenant' => $this->getTenant()->getId(),
            'booking_object' => $this->getBookingObject()->getId()
        ];
    }


}