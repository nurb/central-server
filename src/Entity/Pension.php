<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 */
class Pension extends BookingObject
{
    /**
     * @var string
     * @ORM\Column(type="boolean", length=128)
     */
    private $laundry;

    /**
     * @var string
     * @ORM\Column(type="boolean", length=128)
     */
    private $breakfast;

    /**
     * @param bool $laundry
     * @return Pension
     */
    public function setLaundry(bool $laundry): Pension
    {
        $this->laundry = $laundry;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLaundry(): ?bool
    {
        return $this->laundry;
    }

    /**
     * @param bool $breakfast
     * @return Pension
     */
    public function setBreakfast(bool $breakfast): Pension
    {
        $this->breakfast = $breakfast;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBreakfast(): ?bool
    {
        return $this->breakfast;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'contact' => $this->getContact(),
            'address' => $this->getAddress(),
            'rates' => $this->getRates(),
            'number_of_rooms' => $this->getNumberOfRooms(),
            'type' => $this->getType(),
            'breakfast' => $this->isBreakfast(),
            'laundry' => $this->isLaundry(),
            'bookings' => $this->getBookings()
        ];
    }
}