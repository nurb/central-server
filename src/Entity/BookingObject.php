<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ORM\Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"BookingObject" = "BookingObject", "Cottage" = "Cottage", "Pension" = "Pension"})
 */
class BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=128)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    protected $contact;

    /**
     * @var string
     * @ORM\Column(type="integer", length=32)
     */
    protected $numberOfRooms;

    /**
     * @var string
     * @ORM\Column(type="string", length=512)
     */
    protected $address;

    /**
     * @var string
     * @ORM\Column(type="string", length=64)
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="decimal", length=64)
     */
    protected $rates;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client", inversedBy="my_objects")
     */
    protected $owner;

    /**
     * @var string
     * @ORM\OneToMany(targetEntity="App\Entity\BookingChessmate", mappedBy="booking_object")
     */
    protected $bookings;


    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    /**
     * @param string $name
     * @return BookingObject
     */
    public function setName(string $name): BookingObject
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $contact
     * @return BookingObject
     */
    public function setContact(string $contact): BookingObject
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return string
     */
    public function getContact(): ?string
    {
        return $this->contact;
    }

    /**
     * @param string $address
     * @return BookingObject
     */
    public function setAddress(string $address): BookingObject
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param float $rates
     * @return BookingObject
     */
    public function setRates(float $rates): BookingObject
    {
        $this->rates = $rates;
        return $this;
    }

    /**
     * @return float
     */
    public function getRates(): ?float
    {
        return $this->rates;
    }

    /**
     * @param mixed $numberOfRooms
     * @return BookingObject
     */
    public function setNumberOfRooms($numberOfRooms)
    {
        $this->numberOfRooms = $numberOfRooms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberOfRooms()
    {
        return $this->numberOfRooms;
    }

    /**
     * @param string $type
     * @return BookingObject
     */
    public function setType(string $type): BookingObject
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param $owner
     * @return BookingObject
     */
    public function setOwner($owner): BookingObject
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwner(): string
    {
        return $this->owner;
    }

    /**
     * @return mixed
     */
    public function getBookings()
    {
        $bookings = [];
        foreach ($this->bookings as $booking) {
            $bookings[] = ['id' => $booking->getId(),
                'check-in' => $booking->getCheckIn()->format('Y-m-d'),
                'check-out' => $booking->getCheckOut()->format('Y-m-d'),
                'tenant' => $booking->getTenant()->getEmail(),
                'booking_object' => $booking->getBookingObject()->getId()];
        }
        return $bookings;
    }

    public function addBookings($bookings)
    {
        $this->bookings->add($bookings);
        return $this;
    }
}