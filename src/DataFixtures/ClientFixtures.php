<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ClientFixtures extends Fixture
{
    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public const CLIENT_ONE = 'Morty';

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;
    }

    public function load(ObjectManager $manager)
    {
        $tenant1 = new Client();
        $tenant1->setEmail('rick@gmail.com');
        $tenant1->setPassport('AN123456');
        $tenant1->setPassword($this->clientHandler->encodePlainPassword('qwerty'));
        $tenant1->setRoles(["ROLE_USER","ROLE_TENANT"]);
        $manager->persist($tenant1);

        $tenant2 = new Client();
        $tenant2->setEmail('nolocal@gmail.com');
        $tenant2->setPassport('AL123456');
        $tenant2->setPassword($this->clientHandler->encodePlainPassword('qwerty'));
        $tenant2->setRoles(["ROLE_USER","ROLE_TENANT"]);
        $manager->persist($tenant2);


        $landlord1 = new Client();
        $landlord1->setEmail('morty@gmail.com');
        $landlord1->setPassport('AN654321');
        $landlord1->setPassword($this->clientHandler->encodePlainPassword('qwerty'));
        $landlord1->setRoles(["ROLE_USER","ROLE_LANDLORD"]);

        $manager->persist($landlord1);
        $this->addReference(self::CLIENT_ONE, $landlord1);
        $manager->flush();
    }
}
