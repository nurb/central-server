<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\BookingObject\BookingObjectType;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ObjectFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * @var ClientHandler
     */
    private $bookingObjectHandler;

    public function __construct(BookingObjectHandler $bookingObjectHandler)
    {

        $this->bookingObjectHandler = $bookingObjectHandler;
    }

    public function load(ObjectManager $manager)
    {
        $pension = $this->bookingObjectHandler->createNewPension([
            'name' => 'Karven',
            'contact' => '0550 123123',
            'address' => '42.64202041984265, 77.08450683593753',
            'rates' => '5000',
            'type' => BookingObjectType::PENSION,
            'number_of_rooms' => '10',
            'breakfast' => true,
            'laundry' => true,
            'owner' => 'morty@gmail.com'
        ]);

        $cottage = $this->bookingObjectHandler->createNewCottege([
            'name' => 'Радуга',
            'contact' => '0550 773656',
            'address' => '42.64202041984265, 77.08450683593753',
            'rates' => '3000',
            'type' => BookingObjectType::COTTAGE,
            'number_of_rooms' => '5',
            'kitchen' => true,
            'terrace' => true,
            'owner' => 'morty@gmail.com'
        ]);

        $manager->persist($pension);
        $manager->persist($cottage);
        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return array(
            ClientFixtures::class
        );
    }
}
