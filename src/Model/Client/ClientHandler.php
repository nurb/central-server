<?php

namespace App\Model\Client;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;

class ClientHandler
{
    const SOC_NETWORK_VKONTAKTE = "vkontakte";
    const SOC_NETWORK_FACEBOOK = "facebook";
    const SOC_NETWORK_GOOGLE = "google";

    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data)
    {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = $this->encodePlainPassword($data['password']);
        $client->setRoles($data['roles']);
        $client->setPassword($password);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);

        return $client;
    }

    /**
     * @param array $data
     * @param Client $client
     * @return Client
     * @internal param ObjectManager $manager
     */
    public function updateClient(array $data, Client $client)
    {
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $client->setPassword($data['password']);
        $client->setVkId($data['vkId'] ?? null);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        return $client;
    }

    /**
     * @param string $type
     * @param string $id
     * @param ClientRepository $clientRepository
     * @return Client|null $client Client
     */
    public function uLoginAuth(string $type, string $id, ClientRepository $clientRepository): ?Client
    {
        switch ($type) {
            case self::SOC_NETWORK_VKONTAKTE:
                $client = $clientRepository->findOneByVk($id);
                break;
            case self::SOC_NETWORK_GOOGLE:
                $client = $clientRepository->findOneByGoogle($id);
                break;
            case self::SOC_NETWORK_FACEBOOK:
                $client = $clientRepository->findOneByFacebook($id);
                break;
            default:
                $client = null;
                break;
        }

        return $client;
    }


    /**
     * @param string $password
     * @return string
     */
    public function encodePlainPassword(string $password): string
    {
        return md5($password) . md5($password . '2');
    }
}
