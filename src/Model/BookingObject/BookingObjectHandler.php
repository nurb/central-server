<?php
/**
 * Created by PhpStorm.
 * User: nurb
 * Date: 02.07.2018
 * Time: 19:24
 */

namespace App\Model\BookingObject;


use App\Entity\Cottage;
use App\Entity\Pension;
use App\Repository\ClientRepository;

class BookingObjectHandler
{

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param array $data
     * @return Pension
     */
    public function createNewPension(array $data)
    {
        $pension = new Pension();
        $pension->setName($data['name']);
        $pension->setContact($data['contact']);
        $pension->setAddress($data['address']);
        $pension->setRates($data['rates']);
        $pension->setType($data['type']);
        $pension->setNumberOfRooms($data['number_of_rooms']);
        $pension->setBreakfast($data['breakfast']);
        $pension->setLaundry($data['laundry']);
        $pension->setOwner($this->clientRepository->findOneByEmail($data['owner']));

        return $pension;
    }

    /**
     * @param array $data
     * @return Cottage
     */
    public function createNewCottege(array $data)
    {
        $cottage = new Cottage();
        $cottage->setName($data['name']);
        $cottage->setContact($data['contact']);
        $cottage->setAddress($data['address']);
        $cottage->setRates($data['rates']);
        $cottage->setType($data['type']);
        $cottage->setNumberOfRooms($data['number_of_rooms']);
        $cottage->setKitchen($data['kitchen']);
        $cottage->setTerrace($data['terrace']);
        $cottage->setOwner($this->clientRepository->findOneByEmail($data['owner']));

        return $cottage;
    }
}