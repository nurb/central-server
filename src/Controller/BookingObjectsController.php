<?php

namespace App\Controller;


use App\Entity\BookingChessmate;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\BookingObject\BookingObjectType;
use App\Repository\BookingChessmateRepository;
use App\Repository\BookingObjectRepository;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Objects controller.
 *
 * @Route("booking-object")
 */
class BookingObjectsController extends Controller
{

    /**
     * @Route("/add/{type}", name="app_create_object")
     * @Method("POST")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param BookingObjectHandler $bookingObjectHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @param string $type
     * @return JsonResponse
     */
    public function createObjectAction(
        BookingObjectRepository $bookingObjectRepository,
        BookingObjectHandler $bookingObjectHandler,
        ObjectManager $manager,
        Request $request,
        string $type
    )
    {
        $data['name'] = $request->request->get('name');
        $data['contact'] = $request->request->get('contact');
        $data['rates'] = $request->request->get('rates');
        $data['address'] = $request->request->get('address');
        $data['type'] = $request->request->get('type');
        $data['number_of_rooms'] = $request->request->get('number_of_rooms');
        $data['owner'] = $request->request->get('owner');

        if (empty($data['name'])
            || empty($data['contact'])
            || empty($data['rates'])
            || empty($data['address'])
            || empty($data['type'])
            || empty($data['number_of_rooms'])
            || empty($data['owner'])
        ) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: ' . var_export($data, 1)], 406);
        }

        if ($bookingObjectRepository->findOneByName($data['name'])) {
            return new JsonResponse(['error' => 'Объект уже существует'], 406);
        }

        switch ($type) {
            case BookingObjectType::COTTAGE:
                $data['kitchen'] = $request->request->get('kitchen');
                $data['terrace'] = $request->request->get('terrace');
                $object = $bookingObjectHandler->createNewCottege($data);
                break;
            case BookingObjectType::PENSION:
                $data['breakfast'] = $request->request->get('breakfast');
                $data['laundry'] = $request->request->get('laundry');
                $object = $bookingObjectHandler->createNewPension($data);
                break;
            default:
                return new JsonResponse(['error' => 'Укажите тип объекта'], 406);
                break;
        }

        $manager->persist($object);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }


    /**
     * @Route("/show", name="app_search_objects")
     * @Method("POST")
     * @param BookingObjectRepository $bookingObjectRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function searchObjectsAction(
        BookingObjectRepository $bookingObjectRepository,
        Request $request
    )
    {
        $name = $request->request->get('name') ?: "";
        $type = $request->request->get('type') ?: "";
        $min_rate = $request->request->get('min_rate') ?: 0;
        $max_rate = $request->request->get('max_rate') ?: 999999;

        $results = $bookingObjectRepository->getAllObjectByCriteria($name, $type, $min_rate, $max_rate);
        $array = [];
        foreach ($results as $result) {
            $array[] = $result->toArray();
        }

        return new JsonResponse($array);
    }

    /**
     * @Route("/show/{id}", requirements={"id": "\d+"}, name="showOneObject")
     * @Method({"GET"})
     * @param int $id
     * @param BookingObjectRepository $bookingObjectRepository
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     * @internal param BookingChessmateRepository $bookingChessmateRepository
     */
    public function showOneObjectAction(
        int $id,
        BookingObjectRepository $bookingObjectRepository
    )
    {
        $results = $bookingObjectRepository->findOneById($id);
        $array = [];
        foreach ($results as $result) {
            $array[] = $result->toArray();
        }
        return new JsonResponse($array[0]);
    }


    /**
     * @Route("/actual/{email}", name="actualBooking")
     * @Method({"HEAD"})
     * @param string $email
     * @param BookingChessmateRepository $bookingChessmateRepository
     * @param ClientRepository $clientRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function actualBookingAction(
        string $email,
        BookingChessmateRepository $bookingChessmateRepository,
        ClientRepository $clientRepository
    )
    {
        $client = $clientRepository->findOneByEmail($email);

        if ($bookingChessmateRepository->findActualBookingByClient($client)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/list/{object_id}/{$limit}", name="actualBooking")
     * @Method({"HEAD"})
     * @param string $limit
     * @param string $object_id
     * @param BookingChessmateRepository $bookingChessmateRepository
     * @param BookingObjectRepository $bookingObjectRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listOfBookingsAction(
        string $limit,
        string $object_id,
        BookingChessmateRepository $bookingChessmateRepository,
        BookingObjectRepository $bookingObjectRepository
    )
    {
        $response=[];
        $object = $bookingObjectRepository->findOneById($object_id);
        $bookings = $bookingChessmateRepository->findLastBooking($object, $limit);

        foreach ($bookings as $booking) {
            $response[] = $booking->toArray();
        }

        return new JsonResponse($response);
    }


    /**
     * @Route("/book", name="bookObject")
     * @Method({"POST"})
     * @param BookingObjectRepository $bookingObjectRepository
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     * @internal param BookingChessmateRepository $bookingChessmateRepository
     */
    public function bookObjectAction(
        BookingObjectRepository $bookingObjectRepository,
        Request $request,
        ClientRepository $clientRepository,
        ObjectManager $manager
    )
    {
        $booking = new BookingChessmate();
        $booking->setCheckIn(new \DateTime($request->request->get("check-in")));
        $booking->setCheckOut(new \DateTime($request->request->get("check-out")));
        $client = $clientRepository->findOneByEmail($request->request->get('tenant'));
        $booking->setTenant($client);
        $object = $bookingObjectRepository->findOneById($request->request->get('object'));
        $booking->setBookingObject($object[0]);
        $manager->persist($booking);
        $manager->flush();
        return new JsonResponse(['result' => 'ok']);
    }

}