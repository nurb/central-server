<?php
/**
 * Created by PhpStorm.
 * User: nurb
 * Date: 06.07.2018
 * Time: 03:16
 */

namespace App\Controller;

use App\Repository\BookingChessmateRepository;
use App\Repository\BookingObjectRepository;

use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Clients controller.
 *
 * @Route("test")
 */
class TestAction extends Controller
{
    /**
     * @Route("/one", name="test")
     * @param Request $request
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function testAction(Request $request, BookingObjectRepository $bookingObjectRepository)
    {
        $name = "";
        $type = "";
        $min_rate = 0;
        $max_rate = 999999;

        $results = $bookingObjectRepository->getAllObjectByCriteria($name, $type, $min_rate, $max_rate);
        $array = [];
        foreach ($results as $result) {
            $array[] = $result->toArray();
        }

        return new JsonResponse($array);
    }

    /**
     * @Route("/two", name="test")
     * @param Request $request
     * @param BookingChessmateRepository $bookingChessmateRepository
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function testTwoAction(Request $request, BookingChessmateRepository $bookingChessmateRepository, ClientRepository $clientRepository)
    {
        $name = "";
        $type = "";
        $min_rate = 0;
        $max_rate = 999999;

        $client = $clientRepository->find(2);
        $response = $bookingChessmateRepository->findActualBookingByClient($client);

        dump($response);

        return new JsonResponse();
    }

    /**
     * @Route("/3", name="test_3")
     * @param Request $request
     * @param BookingChessmateRepository $bookingChessmateRepository
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     * @internal param ClientRepository $clientRepository
     */
    public function testThreeAction(Request $request, BookingChessmateRepository $bookingChessmateRepository, BookingObjectRepository $bookingObjectRepository)
    {
        $response = [];
        $object = $bookingObjectRepository->findOneById(1);
        $bookings = $bookingChessmateRepository->findLastBooking($object, 5);


        foreach ($bookings as $booking) {
            $response[] = $booking->toArray();
        }


        return new JsonResponse($response);

    }

}