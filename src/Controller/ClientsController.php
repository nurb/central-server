<?php

namespace App\Controller;


use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Clients controller.
 *
 * @Route("client")
 */
class ClientsController extends Controller
{
    /**
     * @Route("/password/encode", name="app_client_password_encode")
     * @param ClientHandler $clientHandler
     * @param Request $request
     * @return JsonResponse
     */
    public function passwordEncodeAction(
        ClientHandler $clientHandler,
        Request $request
    )
    {
        return new JsonResponse(
            [
                'result' => $clientHandler->encodePlainPassword(
                    $request->query->get('plainPassword')
                )
            ]
        );
    }

    /**
     * @Route("/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportAndEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/reg", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['roles'] = $request->request->get('roles');

        if (empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: ' . var_export($data, 1)], 406);
        }

        if ($clientRepository->findOneByPassportAndEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'], 406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/update", name="app_update_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function updateClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');

        $client = $clientRepository->findOneByEmail($data['email']);

        if ($client) {
            $client = $clientHandler->updateClient($data, $client);
            $manager->persist($client);
            $manager->flush();
        } else {
            return new JsonResponse(['error' => 'Клиента не существует'], 404);
        }

        return new JsonResponse(['result' => 'ok']);
    }


    /**
     * @Route("/check_client_credentials/{email}/{encodedPassword}", name="app_check_client_credentials")
     * @Method({"HEAD", "GET"})
     * @param string $email
     * @param string $encodedPassword
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientCredentialsAction(
        string $email,
        string $encodedPassword,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->getOneByEmailAndPassword($email, $encodedPassword)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/email/{email}", name="app_client_by_email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByEmailAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $client = $clientRepository->findOneByEmail($email);
        if ($client) {
            return new JsonResponse($client->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }


    /**
     * @Route("/social_id/{type}/{id}", name="app_client_by_social")
     * @Method("GET")
     * @param string $type
     * @param string $id
     * @param ClientHandler $clientHandler
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     * @internal param string $email
     */
    public function clientBySocialIdAction(
        string $type,
        string $id,
        ClientHandler $clientHandler,
        ClientRepository $clientRepository)
    {
        $client = $clientHandler->uLoginAuth($type, $id, $clientRepository);
        if ($client) {
            return new JsonResponse($client->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }


}
